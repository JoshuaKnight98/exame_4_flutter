import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Domicilios SV',
      home: MapSample(),
    );
  }
}

// ignore: use_key_in_widget_constructors
class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  // ignore: prefer_final_fields
  Completer<GoogleMapController> _controller = Completer();

  // ignore: prefer_const_constructors
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: const LatLng(13.98478069951965, -89.56194782037545),
    zoom: 10,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Domicilios SV  25-2880-2017'),
          backgroundColor: Colors.green,
        ),
        body: GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: _kGooglePlex,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          markers: {
            Marker(
                markerId: const MarkerId('Chalchuapa'),
                position: const LatLng(13.986855, -89.677491),
                icon: BitmapDescriptor.defaultMarkerWithHue(
                    BitmapDescriptor.hueViolet),
                infoWindow: const InfoWindow(
                    title: 'Sucursal de Chalchuapa',
                    snippet: 'Sucursal de Chalchuapa, Santa Ana')),
            Marker(
                markerId: const MarkerId('Congo'),
                position: const LatLng(13.903884641275216, -89.49970528680788),
                icon: BitmapDescriptor.defaultMarkerWithHue(
                    BitmapDescriptor.hueOrange),
                infoWindow: const InfoWindow(
                    title: 'Sucursal del Congo',
                    snippet: 'Sucursal del Congo, Santa Ana'))
          },
        ));
  }
}
